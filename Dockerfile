FROM python:3.9

# Copy source code to /app directory in the container
COPY . /app

# Change working directory
WORKDIR /app

# Install dependencies
RUN pip install -r requirements.txt

# Create DB
RUN flask create-db

# RUN python -m unittest

# Expose API port to the outside
EXPOSE 5000

# Launch application
ENV FLASK_APP=app.py
CMD ["python","-m", "flask", "run"]
# CMD ["python","-m", "flask", "run", "-h", "0.0.0.0"]
